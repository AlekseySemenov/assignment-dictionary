extern string_equals

global find_word

section .text

find_word:
    mov r8, rsi
    add rsi, 8
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi


    mov rsi, r8
    test rax, rax
    jne .get_word
    mov rsi, [rsi]
    cmp rsi, 0
    je .get_word
    jmp find_word
    
    .get_word:
    mov rax, rsi
    ret
