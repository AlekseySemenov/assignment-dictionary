%include "lib.inc"
%include "colon.inc"
%include "words.inc"

global _start

section .rodata
errorMessage1: db "NOT_FOUND", 0
errorMessage2: db "LONG_WORD", 0

section .bss
word_buffer: resb 256

section .text

_start:
    mov rdi, word_buffer
    mov rsi, 256
    call read_word
    test rax, rax 
    je .print_errorMessage2
    mov rsi, head
    cmp rsi, 0
    je .print_errorMessage1
    mov rdi, rax
    call find_word
    cmp rax, 0
    jne .print_word

    .print_errorMessage1:
    mov rdi, errorMessage1
    call print_error_string
    mov rdi, 1
    jmp .exit

    .print_errorMessage2:
    mov rdi, errorMessage2
    call print_error_string
    mov rdi, 1
    jmp .exit

    .print_word:
    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    mov rdi, 0
        
    .exit:
    call exit
