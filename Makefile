ASM        = nasm
ASMFLAGS   =-felf64 -g

main: main.o dict.o lib.o
	ld -o main main.o dict.o lib.o

main.o: main.asm words.inc colon.inc
	nasm -felf64 -g main.asm

dict.o: dict.asm 
	nasm -felf64 -g dict.asm

lib.o: lib.asm
	nasm -felf64 -g lib.asm
